package com.jmd.test.jsf;

/**
 * https://www.jmdoudoux.fr/java/dej/chap-jsf.htm#jsf-16 copy/paste lines
 * included bug
 */
public class LoginBean {

	private String nom;

	private String mdp;

	public String getMdp() {

		return mdp;

	}

	public String getNom() {

		return nom;

	}

	public void setMdp(String string) {

		mdp = string;

	}

	public void setNom(String string) {

		nom = string;

	}

}